package main_program;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

import data_structures.*;
import data_structures.HashMap;
import data_structures.LinkedHashMap;
import driver.Driver;

/**
 * Created by Walter on 7/22/17.
 */
public class MainProgram {
    public static void main(String[] args) throws FileNotFoundException {
        boolean proceed;
        try {
            do{
                /* Initialisation of objects */
                Driver driver1 = new Driver(new HashMap<>());
                Driver driver2 = new Driver(new ArrayListMap<>());
                Driver driver3 = new Driver(new ListMap<>());
                Driver driver4 = new Driver(new BinaryTree<>());
                Driver driver5 = new Driver(new MyArrayList<>(), new MyArrayList<>());
                Driver driver6 = new Driver(new LinkedHashMap<>());

                java.util.HashMap wordsV5 = new java.util.HashMap();
                File file = getFile(driver1);

                /* Dummy Processing */
                countEachWordsV3(wordsV5, file);
                countEachWordsV3(wordsV5, file);
                /* End of Dummy Processing */

                System.out.print("Choose a data structure" +
                                "\n=======================" +
                                "\n1. Hash Map" +
                                "\n2. ArrayList Hash Map" +
                                "\n3. List Map" +
                                "\n4. Binary Tree Map" +
                                "\n5. Array List" +
                                "\n6. Linked Hash Map" +
                                "\n=====================" +
                                "\nEnter choice (1-6): ");

                Scanner scanner = new Scanner(System.in);
                int choice = Integer.parseInt(scanner.next());
                switch(choice){
                    case 1 :
                        execute(driver1, "HashMap", file);
                        break;
                    case 2 :
                        execute(driver2, "ArrayListMap", file);
                        break;
                    case 3:
                        execute(driver3, "ListMap", file);
                        break;
                    case 4:
                        execute(driver4, "BinaryTreeMap", file);
                        break;
                    case 5:
                        executeArrayList(driver5, "ArrayList", file);
                        break;
                    case 6:
                        execute(driver6, "LinkedHashMap", file);
                        break;
                    default:
                        System.out.println("Invalid choice");
                        break;

                }


                /* Option to proceed to another session */
                System.out.print("Read another file? (y / n) : ");
                Scanner userRequestInput = new Scanner(System.in);
                char userRequest = userRequestInput.next().charAt(0);
                if(userRequest == 'y' || userRequest == 'Y'){
                    proceed = true;
                }else{
                    proceed = false;
                }
                /* End of message prompt */
            }while(proceed);

        }catch(Exception exc){
            System.out.println(exc.getMessage());
            System.out.println("Please restart the program, Thank you :)");
        }
    }

    protected static File getFile(Driver driver){
        return driver.requestFileNameInput();
    }
    static void execute(Driver driver, String structureName, File file){
        driver.printDuration(structureName, driver.getMap(), file);
    }

    static void executeArrayList(Driver driver, String structureName, File file){
        driver.printDuration(structureName, driver.getWord(), driver.getWordCount(), file);
    }
    static void countEachWordsV3(java.util.HashMap words, File file) throws FileNotFoundException{
        Scanner input = new Scanner(file);
        while(input.hasNext()){
            String word = input.next().replaceAll("[^a-zA-Z ]", "").toLowerCase();
            Integer count = (Integer)words.get(word);
            if(count == null){
                count = 1;
            }else{
                count++;
            }
            words.put(word, count);
        }
        input.reset();
        input.close();
    }
}
