package data_structures;

import java.util.*;

public abstract class MyAbstractMap<K, V> implements MyMap<K,V>{
    // The number of entries in the map
    private int size = 0;

    protected MyAbstractMap() { }

    /** Remove all of the entries from this map */
    @Override
    public void clear() {
        size = 0;
    }

    @Override
    public boolean containsKey(K key) {
        if (get(key) != null)
            return true;
        else
            return false;
    }

    /** Returns true if it contains the value */
    @Override
    public abstract boolean containsValue(V value);

    @Override /** Return the value that matches the specified key */
    public abstract V get(K key);

    @Override /** Return true if this map contains no entries */
    public boolean isEmpty() {
        return size == 0;
    }

    @Override /** Return a set consisting of the keys in this map */
    public abstract Set<K> keySet();

    @Override /** Add an entry (key, value) into the map */
    public abstract V put(K key, V value);

    @Override /** Remove the entries for the specified key */
    public abstract void remove(K key);

    @Override /** Return the number of entries in this map */
    public int size() {
        return size;
    }

    @Override /** Return a set consisting of the values in this map */
    public abstract Set<V> values();

    @Override
    public abstract String toString();

    /** prints sorted key-value pairs in the map */
    public void printSorted(){
        Object[] keys = this.keySet().toArray();
        Arrays.sort(keys);

        Object[] values = new Object[keys.length];
        //StringBuilder builder =  new StringBuilder();

        printHeader();

        for(int i = 0; i < keys.length; i++){
            values[i] = this.get((K)keys[i]);

            String key = (String)keys[i];
            int value = (Integer) values[i];

            System.out.print(key);
            int keyStringSpaceCount = key.length();

            while(keyStringSpaceCount < 50){
                System.out.print(" ");
                keyStringSpaceCount++;
            }

            System.out.print("| ");

            System.out.print(value);
            int valueStringSpaceCount = Integer.toString(value).length();

            while(valueStringSpaceCount < 10){
                System.out.print(" ");
                valueStringSpaceCount++;
            }
            System.out.println("|");
        }

        for(int j = 0; j < 62; j++)
            System.out.print("-");
        System.out.println();
    }

    /** prints the header required in the printSorted() method */
    protected void printHeader(){
        for(int i = 0; i < 62; i++)
            System.out.print("-");

        System.out.println();

        String key_label = "Key";
        String value_label = "Value";

        System.out.print(key_label);
        int keySpaceCount = key_label.length();

        while(keySpaceCount < 50){
            System.out.print(" ");
            keySpaceCount++;
        }
        System.out.print("| ");

        System.out.print(value_label);
        int valueSpaceCount = value_label.length();

        while(valueSpaceCount < 10){
            System.out.print(" ");
            valueSpaceCount++;
        }
        System.out.println("|");
        for(int i = 0; i < 62; i++)
            System.out.print("-");
        System.out.println();
    }
}
