package data_structures;
import java.util.*;

public class LinkedHashMap<K, V> extends MyAbstractMap<K, V> {
    /** Hash table is an array of Entry objects */
    private Entry<K,V>[] table;

    /** Current hash table capacity. Capacity is a power of 2 */
    private int capacity = 32768;

    /** The starting entry of the map */
    private Entry<K,V> header;

    /** The last entry of the map */
    private Entry<K,V> last;

    /** Size of the map */
    private int size = 0;

    /** The number of comparisons */
    private int count = 0;

    /** inner class for Entry */
    static class Entry<K, V>{
        K key;
        V value;
        Entry<K,V> next;
        Entry<K,V> before, after;

        public Entry(K key, V value, Entry<K,V> next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }
    }


    @SuppressWarnings("unchecked")/** Constructor */
    public LinkedHashMap(){
        table = new Entry[capacity];
    }

    /** maintains the order of the data structure after deletion **/
    private void maintainOrderAfterInsert(Entry<K,V> newEntry){
        if(header == null){
            header = newEntry;
            last = newEntry;
            size++;
        }

        if(header.key.equals(newEntry.key)){
            deleteFirst();
            insertFirst(newEntry);
            return;
        }

        if(last.key.equals(newEntry.key)){
            deleteLast();
            insertLast(newEntry);
            return;
        }

        Entry<K, V> beforeDeleteEntry = deleteSpecificEntry(newEntry);
        if(beforeDeleteEntry==null){
            insertLast(newEntry);
        }
        else{
            insertAfter(beforeDeleteEntry,newEntry);
        }
    }

    /** maintains the order of the data structure after deletion **/
    private void maintainOrderAfterDeletion(Entry<K, V> deleteEntry) {

        if(header.key.equals(deleteEntry.key)){
            deleteFirst();
            return;
        }

        if(last.key.equals(deleteEntry.key)){
            deleteLast();
            return;
        }

        deleteSpecificEntry(deleteEntry);

    }

    /**
     * returns entry after which new entry must be added.
     */
    private void insertAfter(Entry<K, V> beforeDeleteEntry, Entry<K, V> newEntry) {
        Entry<K, V> current=header;
        while(current!=beforeDeleteEntry){
            current=current.after;//move to next node.
        }

        newEntry.after=beforeDeleteEntry.after;
        beforeDeleteEntry.after.before=newEntry;
        newEntry.before=beforeDeleteEntry;
        beforeDeleteEntry.after=newEntry;
        size++;
    }

    public Set<K> keySet(){
        Set<K> set = new HashSet<K>();

        Entry<K, V> currentEntry=header;
        while(currentEntry!=null){
            set.add(currentEntry.key);
            currentEntry=currentEntry.after;
        }
        return set;
    }

    @Override /** returns the size of the LinkedHashMap */
    public int size(){
        return size;
    }

    /**
     * deletes entry from first.
     */
    private void deleteFirst(){

        if(header==last){ //only one entry found.
            header=last=null;
            size--;
            return;
        }
        header=header.after;
        header.before=null;
        size--;
    }

    /**
     * inserts entry at first.
     */
    private void insertFirst(Entry<K, V> newEntry){

        if(header==null){ //no entry found
            header=newEntry;
            last=newEntry;
            size++;
            return;
        }

        newEntry.after=header;
        header.before=newEntry;
        header=newEntry;
        size++;
    }

    /**
     * inserts entry at last.
     */
    private void insertLast(Entry<K, V> newEntry){

        if(header==null){
            header=newEntry;
            last=newEntry;
            size++;
            return;
        }
        last.after=newEntry;
        newEntry.before=last;
        last=newEntry;
        size++;
    }

    /**
     * deletes entry from last.
     */
    private void deleteLast(){
        if(header==last){
            header=last=null;
            size--;
            return;
        }

        last=last.before;
        last.after=null;
        size--;
    }

    /**
     * deletes specific entry and returns before entry.
     */
    private Entry<K, V> deleteSpecificEntry(Entry<K, V> newEntry){

        Entry<K, V> current=header;
        while(!current.key.equals(newEntry.key)){
            if(current.after==null){   //entry not found
                return null;
            }
            current=current.after;  //move to next node.
        }

        Entry<K, V> beforeDeleteEntry=current.before;
        current.before.after=current.after;
        current.after.before=current.before;
        size--;//entry deleted
        return beforeDeleteEntry;
    }

    /**
     * Method returns value corresponding to key.
     */
    @Override
    public V get(K key){
        int hash = hash(key);
        if(table[hash] == null){
            return null;
        }else{
            Entry<K,V> temp = table[hash];
            while(temp!= null){
                if(temp.key.equals(key))
                    return temp.value;
                temp = temp.next; //return value corresponding to key.
            }
            return null;   //returns null if key is not found.
        }
    }

    /** Removes key-value pair from LinkedHashMap. */
    public void remove(K deleteKey){

        int hash=hash(deleteKey);

        if(table[hash] == null){
            return ;
        }else{
            Entry<K,V> previous = null;
            Entry<K,V> current = table[hash];

            while(current != null){ //we have reached last entry node of bucket.
                if(current.key.equals(deleteKey)){
                    maintainOrderAfterDeletion(current);
                    if(previous==null){  //delete first entry node.
                        table[hash]=table[hash].next;
                        size--;
                        return ;
                    }
                    else{
                        previous.next=current.next;
                        size--;
                        return ;
                    }
                }
                previous=current;
                current = current.next;
            }
            return ;
        }

    }

    /**
     * Method implements hashing functionality
     */
    private int hash(K key){
        return Math.abs(key.hashCode()) % capacity;
    }

    /** Return the number of comparisons during insertion **/
    public int getCount(){
        return count;
    }

    @Override /** Add an entry (key, value) into the map */
    public V put(K newKey, V data){
        //manageCapacity();
        if(newKey==null) {
            count++;
            return null;    //does not allow to store null.
        }
        int hash = hash(newKey);

        Entry<K,V> newEntry = new Entry<K,V>(newKey, data, null);
        maintainOrderAfterInsert(newEntry);
        if(table[hash] == null){
            count++;
            table[hash] = newEntry;
        }else{
            Entry<K,V> previous = null;
            Entry<K,V> current = table[hash];
            while(current != null){ //we have reached last entry of bucket.
                if(current.key.equals(newKey)){
                    count++;
                    if(previous==null){
                        count++;//node has to be insert on first of bucket.
                        newEntry.next=current.next;
                        table[hash]=newEntry;
                        return null;
                    }
                    else{
                        count++;
                        newEntry.next=current.next;
                        previous.next=newEntry;
                        return null;
                    }
                }
                previous=current;
                current = current.next;
            }
            previous.next = newEntry;
        }
        return data;
    }

    @Override
    public void printSorted(){
        Object[] keys = this.keySet().toArray();
        Arrays.sort(keys);
        Object[] values = new Object[keys.length];

        StringBuilder builder =  new StringBuilder();

        printHeader();

        for(int i = 0; i < keys.length; i++){
            values[i] = this.get((K)keys[i]);

            String key = (String)keys[i];
            int value = (Integer) values[i];

            System.out.print(key);
            int keyStringSpaceCount = key.length();

            while(keyStringSpaceCount < 50){
                System.out.print(" ");
                keyStringSpaceCount++;
            }

            System.out.print("| ");

            System.out.print(value);
            int valueStringSpaceCount = Integer.toString(value).length();

            while(valueStringSpaceCount < 10){
                System.out.print(" ");
                valueStringSpaceCount++;
            }
            System.out.println("|");
        }

        for(int j = 0; j < 62; j++)
            System.out.print("-");
        System.out.println();

    }

    @Override/** Return a String containing all the key-value pairs of the map **/
    public String toString(){
        throw new UnsupportedOperationException();
    }

    @Override /** Return the number of entries in this map */
    public int getSize() {
        return this.size;
    }

    @Override/** Return true if this map contains the value */
    public boolean containsValue(V value){
        throw new UnsupportedOperationException();
    }

    @Override /** Return a set consisting of the values in this map */
    public Set<V> values() {
        throw new UnsupportedOperationException();
    }
}
