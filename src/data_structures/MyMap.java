package data_structures;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created by Walter on 7/23/17.
 */
public interface MyMap<K, V> {
    /** empties all the entries in the map */
    void clear();

    /** obtains the size of the map */
    int getSize();

    /** returns true if the map contains the specified key */
    boolean containsKey(K key);

    /** returns true if the map contains the specified value */
    boolean containsValue(V value);

    /** returns the value with the given key */
    V get(K key);

    /** returns true if the map is empty */
    boolean isEmpty();

    /** returns a set of keys */
    Set<K> keySet();

    /** Add an entry (key, value) into the map */
    V put(K key, V value);

    /** Remove the entries for the specified key */
    void remove(K key);

    /** Return the number of mappings in this map */
    int size();

    /** Return a set consisting of the values in this map */
    Set<V> values();

    /** Define inner class for Entry */
    class Entry<K, V> {
        K key;
        V value;

        public Entry(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }

        @Override
        public String toString() {
            return "[" + key + ", " + value + "]";
        }
    }

    /** prints sorted key-value pairs in the map */
    void printSorted();

    /** returns the number of comparisons done in the put() method */
    int getCount();
}
