package data_structures;


import java.util.HashSet;
import java.util.Set;

public class BinaryTree<K extends Comparable<K>, V> extends AbstractTree<K> implements MyMap<K,V>{
    protected TreeNode<K, V> root;
    protected int size = 0;
    protected int count = 0;
    protected Set<K> set = new HashSet<>();

    /** Create a default binary tree */
    public BinaryTree() {
    }

    /** Create a binary tree from an array of objects */
    public BinaryTree(K[] objects) {
        for (int i = 0; i < objects.length; i++)
            insert(objects[i]);
    }

    /** Returns true if the element is in the tree */
    public boolean search(K k) {
        TreeNode<K, V> current = root; // Start from the root

        while (current != null) {
            if (k.compareTo(current.key) < 0) {
                current = current.left;

            }
            else if (k.compareTo(current.key) > 0) {
                current = current.right;
            }
            else {// element matches current.element

                return true;
            }// Element is found
        }

        return false;
    }
    /** Returns TreeNode if the element key is in the tree */
    public TreeNode<K,V> search_value(K k) {
        TreeNode<K, V> current = root; // Start from the root

        while (current != null) {
            if (k.compareTo(current.key) < 0) {
                current = current.left;
            }
            else if (k.compareTo(current.key) > 0) {
                current = current.right;
            }
            else // element matches current.element
                return current; // Element is found
        }

        return null;
    }

    @Override
    public boolean insert(K k) {
        return false;
    }

    @Override
    public boolean delete(K k) {
        return false;
    }


    /** Insert element o into the binary tree
     * Return true if the element is inserted successfully */

    protected TreeNode<K, V> createNewNode(K k, V v) {
        return new TreeNode<>(k, v);
    }



    /** Inorder traversal from a subtree*/
    protected void keySetInorder(TreeNode<K, V> root) {

        if (root == null) return;
        keySetInorder(root.left);
        System.out.println(root.key);
        set.add(root.key);
        keySetInorder(root.right);
    }
    /** Inorder traversal from the root*/
    protected void keySetInorder() {
        keySetInorder(root);
    }

    public Set<K> keySet() {
        keySetInorder();
        return set;
    }

    /** This inner class is static, because it does not access
     any instance members defined in its outer class */
    public static class TreeNode<K extends Comparable<K>, V> {
        K key;
        V value;
        TreeNode<K, V> left;
        TreeNode<K, V> right;

        public TreeNode(K k, V v) {
            key = k;
            value = v;
        }
        public V getValue(){
            return value;
        }
        public K getKey(){return key;}
        public void setValue(V v){
            this.value = v;
        }
    }

    /** Get the number of nodes in the tree */
    public int getSize() {
        return size;
    }

    /** Returns a path from the root leading to the specified element */
    public java.util.ArrayList<TreeNode<K, V>> path(K k) {
        java.util.ArrayList<TreeNode<K, V>> list =
                new java.util.ArrayList<TreeNode<K, V>>();
        TreeNode<K, V> current = root; // Start from the root

        while (current != null) {
            list.add(current); // Add the node to the list
            if (k.compareTo(current.key) < 0) {
                current = current.left;
            }
            else if (k.compareTo(current.key) > 0) {
                current = current.right;
            }
            else
                break;
        }

        return list; // Return an array of nodes
    }

    /** Insert element o into the binary tree
     * Return Value if the element is inserted successfully */
    public V put(K k, V v) {
        if (root == null) {
            count++;
            size++;
            root = createNewNode(k, v); // Create a new root
            return v;
        } else {
            // Locate the parent node
            TreeNode<K, V> parent = null;
            TreeNode<K, V> current = root;
            while (current != null)
                if (k.compareTo(current.key) < 0) {
                    parent = current;
                    current = current.left;
                    count++;
                }
                else if (k.compareTo(current.key) > 0) {
                    parent = current;
                    current = current.right;
                    count++;
                }
                else{
                    current.setValue(v);
                    count++;
                    return v; // Duplicate node not inserted
                }

            // Create the new node and attach it to the parent node
            if (k.compareTo(parent.key) < 0) {

                parent.left = createNewNode(k, v);

            }
            else {

                parent.right = createNewNode(k, v);

            }
        }
        count++;
        size++;
        return v; // Element inserted
    }

    /** Insert element o into the binary tree
     * Return Value if the element is inserted successfully */

    public V get(K k){
        TreeNode<K, V> current = root; // Start from the root

        while (current != null) {
            if (k.compareTo(current.key) < 0) {
                current = current.left;
            }
            else if (k.compareTo(current.key) > 0) {
                current = current.right;
            }
            else // element matches current.element
                return current.value; // Element is found
        }
        return null;
    }

    /** Remove all elements from the tree */
    public void clear() {
        root = null;
        size = 0;
    }

    public Set<V> values() {
        throw new UnsupportedOperationException();
    }


//    public Set<Entry<K, V>> entrySet() {
//        return null;
//    }
    @Override
    public void printSorted(){
        printHeader();
        printSorted(root);
        for(int j = 0; j < 62; j++)
            System.out.print("-");
        System.out.println();
    }

    protected void printSorted(TreeNode<K,V> root){
        if (root == null) return;
        printSorted(root.left);
        String key = (String)root.key;
        System.out.print(key);
        int keyStringSpaceCount = key.length();

        while(keyStringSpaceCount < 50){
            System.out.print(" ");
            keyStringSpaceCount++;
        }

        System.out.print("| ");

        String value = Integer.toString((Integer) root.value);
        System.out.print(value);

        int valueStringSpaceCount = value.length();

        while(valueStringSpaceCount < 10){
            System.out.print(" ");
            valueStringSpaceCount++;
        }
        System.out.println("|");
        printSorted(root.right);
    }

    public int getCount(){
        return count;
    }

    protected void printHeader(){
        for(int i = 0; i < 62; i++)
            System.out.print("-");

        System.out.println();

        String key_label = "Key";
        String value_label = "Value";

        System.out.print(key_label);
        int keySpaceCount = key_label.length();

        while(keySpaceCount < 50){
            System.out.print(" ");
            keySpaceCount++;
        }
        System.out.print("| ");

        System.out.print(value_label);
        int valueSpaceCount = value_label.length();

        while(valueSpaceCount < 10){
            System.out.print(" ");
            valueSpaceCount++;
        }
        System.out.println("|");
        for(int i = 0; i < 62; i++)
            System.out.print("-");
        System.out.println();
    }

    @Override
    public boolean containsKey(K key){
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean containsValue(V value) {
        return false;
    }

    @Override
    public void remove(K key) { throw new UnsupportedOperationException(); }

    @Override
    public int size() {
        return 0;
    }
}

