package data_structures;

public interface MyList<E> {
     void add(E e);

     void add(int index, E e);

     boolean contains(E e);

     int indexOf(E e);

     boolean isEmpty();

     boolean remove(E e);

     E remove(int index);

     Object set(int index, E e);

     int size();
}
