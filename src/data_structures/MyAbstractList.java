package data_structures;

public abstract class MyAbstractList<E> implements MyList<E>{
    protected int size = 0;

    protected MyAbstractList(){}

    protected MyAbstractList(E[] objects) {
        for (int i = 0; i < objects.length; i++)
            add(objects[i]);
    }

    public void add(E e) {
        add(size, e);
    }

    public boolean contains(E e) {
        return false;
    }

    public int indexOf(E e) {
        return 0;
    }

    public boolean isEmpty() {
        return size == 0;
    }


    public boolean remove(E e) {
        if (indexOf(e) >= 0) {
            remove(indexOf(e));
            return true;
        }
        else
            return false;
    }

    public Object set(int index, E e) {
        return null;
    }

    public int size() {
        return size;
    }
}
