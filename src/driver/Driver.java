package driver;

import data_structures.MyArrayList;
import data_structures.MyMap;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Driver {
    private MyMap<String, Integer> map;
    private MyArrayList<String> word;
    private MyArrayList<Integer> wordCount;
    private String fileName;

    public Driver(){ }

    public Driver(MyMap<String, Integer> map){
        this.map = map;
    }

    public Driver(MyArrayList<String> word, MyArrayList<Integer> wordCount){
        this.word = word;
        this.wordCount = wordCount;
    }

    public MyMap<String, Integer> getMap() {
        return map;
    }

    public MyArrayList<String> getWord() {
        return word;
    }

    public MyArrayList<Integer> getWordCount() {
        return wordCount;
    }

    public File requestFileNameInput(){
        Scanner input = new Scanner(System.in);
        File file;
        if(fileName == null){
            System.out.print("Enter file name in txt format (without the .txt): ");
            fileName = input.nextLine() + ".txt";
            file = new File(fileName);
        }else
            file = new File(fileName);

        return file;
    }

    private void countWordsOccurence(MyMap<String,Integer> map, File textFile) throws FileNotFoundException {
        File file = textFile;
        Scanner input = new Scanner(file);

        while(input.hasNext()){
            String word = input.next().replaceAll("[\\p{Punct}\\s]+", "").toLowerCase();
            //String word = input.next().replaceAll("[ \\n\\t\\r.,;:!?(){}]+", "").toLowerCase();
            Integer count = map.get(word);
            if(count == null){
                count = 1;
            }else{
                count++;
            }
            map.put(word, count);
        }
        input.close();
    }

    //for array list data structure
    private void countWordsOccurence(MyArrayList<String> words, MyArrayList<Integer> wordCount, File textFile) throws FileNotFoundException{
        File file = textFile;
        Scanner input = new Scanner(file);

        while(input.hasNext()){
            String word = input.next().replaceAll("[?!.,;]","").toLowerCase();
            if(words.contains(word)){
                int index = words.indexOf(word);
                wordCount.set(index, wordCount.get(index) +1);
            }
            else{
                words.add(word);
                wordCount.add(1);
            }
        }
    }

    public void printSorted(MyArrayList<String> keyToSort, MyArrayList<Integer>valToSort) {
        String key;
        int value;
        for (int i = 0; i < keyToSort.size() - 1; i++) {
            for (int j = i + 1; j < keyToSort.size(); j++) {
                if (keyToSort.get(i).compareTo(keyToSort.get(j))>0) {
                    key = keyToSort.get(i);
                    keyToSort.set(i, keyToSort.get(j));
                    keyToSort.set(j, key);

                    //sort the index of value to be parallel with key
                    value = valToSort.get(i);
                    valToSort.set(i, valToSort.get(j));
                    valToSort.set(j, value);
                }
            }
        }

        printHeader();

        for(int i = 0; i < keyToSort.size(); i++){
            String keyObtained = keyToSort.get(i);
            System.out.print(keyObtained);
            int keyStringSpaceCount = keyObtained.length();

            while(keyStringSpaceCount < 50){
                System.out.print(" ");
                keyStringSpaceCount++;
            }

            System.out.print("| ");
            int valueObtained = valToSort.get(i);
            System.out.print(valueObtained);
            int valueStringSpaceCount = Integer.toString(valueObtained).length();

            while(valueStringSpaceCount < 10){
                System.out.print(" ");
                valueStringSpaceCount++;
            }
            System.out.println("|");
        }

        for(int j = 0; j < 62; j++)
            System.out.print("-");
        System.out.println();

    }

    private Long getInsertionTime(MyMap<String, Integer> map, File file){
        long timeTakenCountWords = 0;
        try {
            /* measure time taken to count words */
            long startTimeCountWords = System.currentTimeMillis();
            countWordsOccurence(map, file);
            long endTimeCountWords = System.currentTimeMillis();
            timeTakenCountWords = endTimeCountWords - startTimeCountWords;

        }catch (FileNotFoundException exc){
            System.out.println(exc.getMessage());
        }catch (Exception exc){
            System.out.println(exc.getMessage());
        }finally {
            return timeTakenCountWords;
        }
    }

    private Long getInsertionTime(MyArrayList<String> words, MyArrayList<Integer> wordCount, File file){
        long timeTakenCountWords = 0;
        try {
            /* measure time taken to count words */
            long startTimeCountWords = System.currentTimeMillis();
            countWordsOccurence(words, wordCount, file);
            long endTimeCountWords = System.currentTimeMillis();
            timeTakenCountWords = endTimeCountWords - startTimeCountWords;

        }catch (FileNotFoundException exc){
            System.out.println(exc.getMessage());
        }catch (Exception exc){
            System.out.println(exc.getMessage());
        }finally {
            return timeTakenCountWords;
        }
    }


    private Long getSortTime(MyMap<String, Integer> map){
        long timeTakenSortWords = 0;
        try {
            /* measure time taken for sort words */
            long startTimeSortWords = System.currentTimeMillis();
            map.printSorted();
            long endTimeSortWords = System.currentTimeMillis();
            timeTakenSortWords = endTimeSortWords - startTimeSortWords;
        }catch (Exception exc){
            System.out.println(exc.getMessage());
        }finally {
            return timeTakenSortWords;
        }
    }

    private Long getSortTime(MyArrayList<String> words, MyArrayList<Integer> wordCount){
        long timeTakenSortWords = 0;
        try {
            /* measure time taken for sort words */
            long startTimeSortWords = System.currentTimeMillis();
            printSorted(words, wordCount);
            long endTimeSortWords = System.currentTimeMillis();
            timeTakenSortWords = endTimeSortWords - startTimeSortWords;
        }catch (Exception exc){
            System.out.println(exc.getMessage());
        }finally {
            return timeTakenSortWords;
        }
    }

    public void printDuration(String structureName, MyMap<String, Integer> map, File file){
        long countTime = getInsertionTime(map, file);
        long sortTime = getSortTime(map);
        System.out.println("*** Time taken for " + structureName + " to complete counting : " + countTime + "ms ***");
        System.out.println("*** Time taken for " + structureName + " to complete sorting : " + sortTime + "ms ***");
        System.out.println("*** Total Comparisons: " + map.getCount() + " ***");
        System.out.println("*** Total distinct words: " + map.getSize() + " ***");
    }

    public void printDuration(String structureName, MyArrayList<String> word, MyArrayList<Integer> wordCount, File file){
        long countTime = getInsertionTime(word, wordCount, file);
        long sortTime = getSortTime(word, wordCount);
        System.out.println("*** Time taken for " + structureName + " to complete counting : " + countTime + "ms ***");
        System.out.println("*** Time taken for " + structureName + " to complete sorting : " + sortTime + "ms ***");
        System.out.println("*** Total Comparisons: " + word.getNumOfComparison()+" ***");
        System.out.println("*** Total distinct words: " + word.size() + " ***");
    }

    protected void printHeader(){
        for(int i = 0; i < 62; i++)
            System.out.print("-");

        System.out.println();

        String key_label = "Key";
        String value_label = "Value";

        System.out.print(key_label);
        int keySpaceCount = key_label.length();

        while(keySpaceCount < 50){
            System.out.print(" ");
            keySpaceCount++;
        }
        System.out.print("| ");

        System.out.print(value_label);
        int valueSpaceCount = value_label.length();

        while(valueSpaceCount < 10){
            System.out.print(" ");
            valueSpaceCount++;
        }
        System.out.println("|");
        for(int i = 0; i < 62; i++)
            System.out.print("-");
        System.out.println();
    }
}
